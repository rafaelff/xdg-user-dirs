# Friulian translation for xdg-user-dirs.
# Copyright (C) 2016 xdg-user-dirs's COPYRIGHT HOLDER
# This file is distributed under the same license as the xdg-user-dirs package.
# Fabio Tomat <f.t.public@gmail.com>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: xdg-user-dirs 0.14\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-03-14 10:09+0100\n"
"PO-Revision-Date: 2016-11-16 14:28+0100\n"
"Last-Translator: Fabio Tomat <f.t.public@gmail.com>\n"
"Language-Team: Friulian <f.t.public@gmail.com>\n"
"Language: fur\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"X-Generator: Poedit 1.8.8\n"

#: translate.c:2
msgid "Applications"
msgstr "Aplicazions"

#: translate.c:2
msgid "applications"
msgstr "aplicazions"

#: translate.c:3
msgid "Desktop"
msgstr "Scritori"

#: translate.c:3
msgid "desktop"
msgstr "scritori"

#: translate.c:4
msgid "Documents"
msgstr "Documents"

#: translate.c:4
msgid "documents"
msgstr "documents"

#: translate.c:5
msgid "Download"
msgstr "Discjariâts"

#: translate.c:5
msgid "download"
msgstr "discjariâts"

#: translate.c:6
msgid "Downloads"
msgstr "Discjariâts"

#: translate.c:6
msgid "downloads"
msgstr "discjariâts"

#: translate.c:7
msgid "Movies"
msgstr "Filmâts"

#: translate.c:7
msgid "movies"
msgstr "filmâts"

#: translate.c:8
msgid "Music"
msgstr "Musiche"

#: translate.c:8
msgid "music"
msgstr "musiche"

#: translate.c:9
msgid "Photos"
msgstr "Fotos"

#: translate.c:9
msgid "photos"
msgstr "fotos"

#: translate.c:10
msgid "Pictures"
msgstr "Imagjins"

#: translate.c:10
msgid "pictures"
msgstr "imagjins"

#: translate.c:11
msgid "Projects"
msgstr "Progjets"

#: translate.c:11
msgid "projects"
msgstr "progjets"

#: translate.c:12
msgid "Public"
msgstr "Public"

#: translate.c:12
msgid "public"
msgstr "public"

#: translate.c:13
msgid "Share"
msgstr "Condividût"

#: translate.c:13
msgid "share"
msgstr "condividût"

#: translate.c:14
msgid "Templates"
msgstr "Modei"

#: translate.c:14
msgid "templates"
msgstr "modei"

#: translate.c:15
msgid "Videos"
msgstr "Videos"

#: translate.c:15
msgid "videos"
msgstr "videos"
